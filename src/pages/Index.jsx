import HomepageService from "common/services/homepage.service";
import DiskSpaceCircle from "components/Chart/DiskSpaceCircle";
import Layout from "components/Layout/Layout";
import Title from "components/Title/Title";
import { useEffect, useState } from "react";
import {
  IoBusinessOutline,
  IoCogOutline,
  IoFileTrayOutline,
  IoPeopleOutline,
} from "react-icons/io5";
import { NavLink } from "react-router-dom";

const Index = () => {
  const [info, setInfo] = useState([]);

  useEffect(() => {
    HomepageService.getInfo()
      .then((res) => {
        setInfo(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <Layout>
      <section className="flex xl:flex-row flex-col w-full items-start">
        <main className="grid grid-cols-12 xl:gap-8 gap-5 my-2 w-full">
          <aside className="col-span-12 xl:col-span-6">
            <NavLink to="/new/markets" className="main-tab">
              <div className="flex flex-col">
                <Title>Magazin</Title>

                <div className="text-slate-400">
                  <small className="mr-1">Taze:</small>
                  <small>{info?.new_market_products}</small>
                </div>

                <div className="text-slate-400">
                  <small className="mr-1">Jemi:</small>
                  <small>{info?.total_market_products}</small>
                </div>
              </div>
              <IoBusinessOutline size={48} />
            </NavLink>
          </aside>

          <aside className="col-span-12 xl:col-span-6">
            <NavLink to="/new/services" className="main-tab">
              <div className="flex flex-col">
                <Title>Hyzmat</Title>

                <div className="text-slate-400">
                  <small className="mr-1">Taze:</small>
                  <small>{info?.new_service_products}</small>
                </div>

                <div className="text-slate-400">
                  <small className="mr-1">Jemi:</small>
                  <small>{info?.total_service_products}</small>
                </div>
              </div>
              <IoCogOutline size={48} />
            </NavLink>
          </aside>

          <aside className="col-span-12 xl:col-span-6">
            <NavLink to="/new/services" className="main-tab">
              <div className="flex flex-col">
                <Title>Faýllar</Title>

                <div className="text-slate-400">
                  <small className="mr-1">Taze:</small>
                  <small>{info?.new_files}</small>
                </div>

                <div className="text-slate-400">
                  <small className="mr-1">Jemi:</small>
                  <small>{info?.total_files}</small>
                </div>
              </div>
              <IoFileTrayOutline size={48} />
            </NavLink>
          </aside>

          <aside className="col-span-12 xl:col-span-6">
            <NavLink to="/new/services" className="main-tab">
              <div className="flex flex-col">
                <Title>Agzalar</Title>

                <div className="text-slate-400">
                  <small className="mr-1">Taze:</small>
                  <small>{info?.new_users}</small>
                </div>

                <div className="text-slate-400">
                  <small className="mr-1">Jemi:</small>
                  <small>{info?.total_users}</small>
                </div>
              </div>
              <IoPeopleOutline size={48} />
            </NavLink>
          </aside>
        </main>

        <main className="flex xl:flex-col items-center w-full pl-10 xl:w-[28rem] gap-8 xl:gap-2">
          <DiskSpaceCircle
            disk_available={info?.disk_available}
            disk_used={info?.disk_used}
            disk_usage_percent={info?.disk_usage_percent}
            disk_total={info?.disk_total}
          />
        </main>
      </section>
    </Layout>
  );
};

export default Index;
