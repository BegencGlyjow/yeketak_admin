import api from "common/config/api.service";
import Layout from "components/Layout/Layout";
import Title from "components/Title/Title";
import { useState } from "react";
import FileLoader from "components/Loader/FileLoader";
import { useNavigate } from "react-router-dom";
import { IoImagesOutline } from "react-icons/io5";

const AddArtist = () => {
  const navigate = useNavigate();
  const [progress, setProgress] = useState(0);
  const [formState, setFormState] = useState({
    name: "",
    biography: "",
    image: "",
  });

  const handleChange = (event) => {
    setFormState({ ...formState, [event.target.name]: event.target.value });
  };

  const inputFileOnChange = (e) => {
    setFormState({
      ...formState,
      [e.target.name]: e.target.files[0],
      [e.target.name + "_name"]: e.target.files[0].name,
      name: e.target.name === "file" ? e.target.files[0].name : formState.name,
      [e.target.name + "_size"]: e.target.files[0].size,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const formData = new FormData();

    formData.append("name", formState.name);
    formData.append("image", formState.image);
    formData.append("biography", formState.biography);

    api
      .post("artists", formData, {
        headers: {
          "Content-type": "multipart/form-data",
        },
        onUploadProgress: (e) => {
          let progressCompleted = Math.round((e.loaded * 100) / e.total);
          setProgress(progressCompleted);
        },
      })
      .then(() => {
        setTimeout(() => {
          navigate("/artists");
        }, 1000);
        setProgress(0);
      })
      .catch((err) => setProgress(0));
  };

  return (
    <>
      {progress > 0 && <FileLoader percentage={progress} />}
      <Layout className="bg-gray-900 rounded-xl xl:px-10">
        <Title> Bagşy goş </Title>

        <section className="grid grid-cols-12 items-start gap-5">
          <aside className="flex flex-col justify-between h-full col-span-12 lg:col-span-4">
            <div>
              <label htmlFor="image">
                {formState.image ? (
                  <img
                    src={URL.createObjectURL(formState.image)}
                    alt="image"
                    className="mx-auto my-5 cursor-pointer rounded-lg"
                  />
                ) : (
                  <IoImagesOutline
                    size={256}
                    className="mx-auto my-5 cursor-pointer"
                  />
                )}
                <input
                  type="file"
                  id="image"
                  className="hidden"
                  name="image"
                  onChange={inputFileOnChange}
                />
              </label>
            </div>
          </aside>

          <form
            onSubmit={handleSubmit}
            className="grid grid-cols-12 col-span-12 lg:col-span-8 gap-x-4 gap-y-7 xl:ml-5 my-5"
          >
            <div className="relative col-span-12 border border-slate-800 group bg-slate-800 rounded-lg">
              <div
                className={`bg-slate-800 absolute left-3 top-3 px-2 ${
                  formState.name.length && "-translate-y-7"
                } transform group-focus-within:-translate-y-7 group-hover:-translate-y-7 group-focus-within:text-yellow-300 rounded-lg duration-500 text-gray-300`}
              >
                Bagşynyň ady
              </div>
              <input
                onChange={handleChange}
                name="name"
                type="text"
                value={formState.name}
                className="bg-transparent p-3 w-full"
              />
            </div>

            <div className="relative col-span-12 border border-slate-800 group bg-slate-800 rounded-lg">
              <div
                className={`bg-slate-800 absolute left-3 top-3 px-2 ${
                  formState.biography.length && "-translate-y-7"
                } transform group-focus-within:-translate-y-7 group-hover:-translate-y-7 group-focus-within:text-yellow-300 rounded-lg duration-500 text-gray-300`}
              >
                Bagşy barada
              </div>
              <textarea
                onChange={handleChange}
                name="biography"
                type="text"
                className="bg-transparent p-3 w-full h-48"
              />
            </div>

            <div className="col-span-12">
              <button className="bg-yellow-300 hover:bg-yellow-500 duration-300 text-slate-900 px-5 py-2.5 w-max font-montserrat-bold rounded-lg">
                Sanawa goş
              </button>
            </div>
          </form>
        </section>
      </Layout>
    </>
  );
};

export default AddArtist;
