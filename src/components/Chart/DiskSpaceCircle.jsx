const DiskSpaceCircle = ({
  disk_available,
  disk_used,
  disk_usage_percent,
  disk_total,
}) => {
  return (
    <div class="single-chart">
      <svg viewBox="0 0 36 36" class="circular-chart red">
        <path
          class="circle-bg"
          d="M18 2.0845
        a 15.9155 15.9155 0 0 1 0 31.831
        a 15.9155 15.9155 0 0 1 0 -31.831"
        />
        <path
          class="circle"
          stroke-dasharray={`${disk_usage_percent}, 100`}
          d="M18 2.0845
        a 15.9155 15.9155 0 0 1 0 31.831
        a 15.9155 15.9155 0 0 1 0 -31.831"
        />

        <text x="18" y="9.35" class="percentage">
          Jemi:
        </text>
        <text x="18" y="12.35" class="percentage">
          {disk_total}
        </text>

        <text x="18" y="17.35" class="percentage">
          Ulanyldy:
        </text>
        <text x="18" y="20.35" class="percentage">
          {disk_used}
        </text>

        <text x="18" y="25.35" class="percentage">
          Boş ýer:
        </text>
        <text x="18" y="28.35" class="percentage">
          {disk_available}
        </text>
      </svg>
    </div>
  );
};

export default DiskSpaceCircle;
